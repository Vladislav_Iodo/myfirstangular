import { Component } from '@angular/core';
import {Router} from "@angular/router";

interface NavButton {
  buttonName: string,
  path: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  navButtons: NavButton[] = [
    {
      buttonName: 'home',
      path: ''
    },
    {
      buttonName: 'saved',
      path: 'saved-films'
    }
  ];

  constructor(private router: Router) {}

  navigate(path: any) {
    this.router.navigate([path])
  }
}
