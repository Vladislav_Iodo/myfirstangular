import {Component, OnInit} from '@angular/core';
import {TopFilmsService} from "../../top-films/top-films.service";

interface Movie {
  position: string
  name: string
  surname: string
  url: string
}

@Component({
  selector: 'app-saved-films',
  templateUrl: './saved-films.component.html',
  styleUrls: ['./saved-films.component.less']
})
export class SavedFilmsComponent implements OnInit {
  savedFilmsList: any;

  constructor(private topFilmsService: TopFilmsService) {
  }

  ngOnInit(): void {
    this.topFilmsService.getFilms().subscribe(({data}: any) => {
      this.savedFilmsList = this.topFilmsService.parseResponse({data})
    });
  }

}
