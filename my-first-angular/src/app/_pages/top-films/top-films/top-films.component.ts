import { Component, OnInit } from '@angular/core';
import {TopFilmsService} from "../top-films.service";

interface Movie {
  name: string
  url: string
}

@Component({
  selector: 'app-top-films',
  templateUrl: './top-films.component.html',
  styleUrls: ['./top-films.component.less']
})
export class TopFilmsComponent implements OnInit {
  moviesList: any;

  constructor(private topFilmsService: TopFilmsService) { }

  ngOnInit(): void {
    this.topFilmsService.getFilms().subscribe(({data}: any) => {
     this.moviesList = this.topFilmsService.parseResponse({data})
    });
  }

}
