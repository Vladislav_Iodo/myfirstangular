import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.less']
})
export class FilmComponent implements OnInit {
  @Input() film: any;

  constructor() { }

  ngOnInit(): void {
  }

}
