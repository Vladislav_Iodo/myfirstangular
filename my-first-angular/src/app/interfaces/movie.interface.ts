export interface Movie {
  position: string
  name: string
  surname: string
  url: string
}
